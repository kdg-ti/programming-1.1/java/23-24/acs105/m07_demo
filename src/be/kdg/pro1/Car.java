package be.kdg.pro1;

import java.util.Objects;

public class Car extends Vehicle {
	private String license;
	private String brand;


	public Car(String license, int speed, String brand) {
		super(speed);
		this.license = license;
		this.brand = brand;
		System.out.println("Making a car " + brand);
	}

	public Car(String license,  String brand) {
		this(license, 0, brand);
	}

	public Car(){
		this("0-AAA-000", 0, "unknown");
	}

	@Override
	public   int increaseSpeed(){
		setSpeed(getSpeed()+10);
		return getSpeed();
	}

	@Override
	public  int decreaseSpeed(){
		setSpeed(getSpeed()-10);
		return getSpeed();
	}

	public String getLicense() {
		return license;
	}



	public String getBrand() {
		return brand;
	}

	@Override
	public String toString() {
		return super.toString() + "Car{" +
			"license='" + license + '\'' +
			", brand='" + brand + '\'' +
			'}';
	}

	@Override
	public boolean equals(Object other){
		if (other instanceof Car c) {
//			return ((license == c.license) || license.equals(c.license)) &&
//				(( brand == c.brand) ||brand.equals(c.brand));
			return Objects.equals(license,c.license) && Objects.equals(brand,c.brand);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode(){
	 return 	Objects.hash(license.hashCode() , brand.hashCode());
	}
}
