package be.kdg.pro1;

public abstract class Vehicle {
	private int speed;


	public Vehicle() {
		this(0);
	}

	public Vehicle(int speed) {
		this.speed = speed;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		if(speed >=0) {
			this.speed = speed;
		}
	}

	public abstract  int increaseSpeed();

	public abstract int decreaseSpeed();

	@Override
	public String toString() {
		return "Vehicle{" +
			"speed=" + speed +
			'}';
	}
}
