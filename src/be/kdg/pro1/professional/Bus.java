package be.kdg.pro1.professional;

import be.kdg.pro1.Car;

import java.util.Objects;

public class Bus extends Car {

	private int capacity;

	public Bus(int speed, String brand, int capacity, String license) {
		super( license,speed,brand);
		this.capacity = capacity;
		System.out.println("Making a bus");
	}

	public Bus( String brand, int capacity, String license) {
		this( 0,brand,capacity,license);
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	@Override
	public int increaseSpeed() {
		setSpeed(getSpeed()+ 5);
		return getSpeed();
	}

	@Override
	public String toString() {
		return "Bus{" +
			super.toString() +
			", capacity=" + capacity+
			'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Bus bus = (Bus) o;
		return capacity == bus.capacity;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), capacity);
	}
}
