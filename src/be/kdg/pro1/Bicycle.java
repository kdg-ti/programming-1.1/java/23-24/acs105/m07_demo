package be.kdg.pro1;

public class Bicycle extends Vehicle{
	@Override
	public int increaseSpeed() {
		 setSpeed(getSpeed() +1);
		return getSpeed();
	}

	@Override
	public int decreaseSpeed() {
		setSpeed(getSpeed() -5);
		return getSpeed();
	}

	@Override
	public String toString() {
		return "Bicycle{} " + super.toString();
	}
}
