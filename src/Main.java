import be.kdg.pro1.*;
import be.kdg.pro1.professional.Bus;

public class Main {
	public static void main(String[] args) {
		Car ford = new Car ("1-JAN-001",0,"Ford");
		ford.increaseSpeed();
		System.out.println("First car: "+ford);
		Bus line182 = new Bus(0,"VanHool",80,"2-KWS-552");
		line182.increaseSpeed();
		System.out.println("Bus is canceled: " + line182);
		System.out.println("Tank: " + new Tank());
		Car theBus = line182;
		System.out.println("Class of line182 is: "+ line182.getClass().getSimpleName());
		System.out.println("Class of theBus is: "+ theBus.getClass().getSimpleName());
		System.out.println("Printing  theBus is: "+ theBus);

		// I can assign without casting to a variable of a superclass
		Object thing = ford;
		System.out.println("Class of thing is: "+ thing.getClass().getSimpleName());
		// I cannot call increaseSpeed on thing.
		// I need to assign the object to a Car variable first
		// I must cast to assign to a variable of a subclass (cast down)
		Car thingIsACar = (Car) thing;
		thingIsACar.increaseSpeed();

		System.out.println(examineObject(thing));
		System.out.println(examineObject(theBus));
		System.out.println(">> examining class with switch:");
		System.out.println(examineObjectWithSwitch(thing));
		System.out.println(examineObjectWithSwitch(theBus));
		System.out.println(examineObjectWithSwitch(null));

		Car mine = new Car ("1-JAN-001",10,"Ford");
		System.out.println("mine == ford " + (mine == ford));
		System.out.println("mine.equals(ford) " + (mine.equals(ford)));

		Vehicle vw = new Car ("T-AXI-007",10,"Mercedes");
		System.out.println(vw);
		Vehicle[] vehicles = {vw , ford,line182,new Bicycle()};
		increaseAllSpeeds(vehicles);
	}

	public static void increaseAllSpeeds(Vehicle [] vehicles){
		System.out.println("Increasing speed of all vehicles");
		for (Vehicle vehicle:vehicles){
			vehicle.increaseSpeed();
			System.out.println(vehicle);
		}
	}

	private static String examineObject(Object object) {
		// Legacy instanceof
		if(object instanceof Car){
			Car itIsACar = (Car) object;
			System.out.println("Legacy instanceof " + itIsACar.getBrand());
		}
		// Java >= 16 instanceof
		if (object instanceof Bus b) {
			return "This is a bus with capacity " + b.getCapacity();

		} else if(object instanceof Car c){
			return "This is a car of brand " + c.getBrand();
		} else   {
				return "Just an object";
			}
		}

	private static String examineObjectWithSwitch(Object object) {
		return switch (object){
			case null -> "NULL passed in!";
			case Bus b -> "This is a bus with capacity " + b.getCapacity();
			case Car c -> "This is a car of brand " + c.getBrand();
			default -> "Just an object";
		};
	}


	}
